haskell-swish (0.10.10.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 15 Jan 2025 01:21:08 +0100

haskell-swish (0.10.9.0-2) unstable; urgency=medium

  * no-changes upload to link against newer GHC;
    closes: bug#1085030, thanks to Ilias Tsitsimpis

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 16 Oct 2024 19:12:36 +0200

haskell-swish (0.10.9.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update watch file:
    + track upstream source (not hackage distribution)
    + avoid bogus tag 0.10.11.1
    + set dversionmangle=auto
    + improve usage config
  * update copyright info:
    + update coverage
    + update Source URI
  * simplify packaging
  * declare compliance with Debian Policy 4.7.0
  * deduplicate description strings,
    using ${source:Synopsis} and ${source:Extended-Description}
  * relax upper GHC bounds for hashable

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 14 Aug 2024 10:16:44 +0200

haskell-swish (0.10.7.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * tighten upper GHC bounds for hashable

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 30 Nov 2023 15:22:05 +0100

haskell-swish (0.10.4.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 24 Jul 2023 14:30:40 +0200

haskell-swish (0.10.3.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.6.2

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 11 Jan 2023 15:33:53 +0100

haskell-swish (0.10.2.0-2) unstable; urgency=medium

  * no-change upload to link against newer base libraries

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 29 Jul 2022 16:09:07 +0200

haskell-swish (0.10.2.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update git-buildpackage config:
    + use DEP-14 git branches
    + enable automatic DEP-14 branch name handling
    + add usage config
  * declare compliance with Debian Policy 4.6.1
  * relax upper GHC bounds for hashable;
    drop GHC bounds for old-locale
  * update copyright info: update coverage
  * fix enable tests;
    add GHC build-dependencies
    for hunit test-framework test-framework-hunit;
    closes: bug#1011913, thanks to Scott Talbert
  * annotate test-only build-dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 29 May 2022 10:41:21 +0200

haskell-swish (0.10.1.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update package relations
    to relax GHC upper bounds for semigroups
  * tighten lintian overrides
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 02 Jan 2022 20:44:28 +0100

haskell-swish (0.10.0.8-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update package relations to relax GHC upper bounds for hashable

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 17 Nov 2021 17:59:37 +0100

haskell-swish (0.10.0.7-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * use semantic newlines in long description
  * update package relations to relax GHC upper bounds for network-uri
  * drop patch 1001 obsoleted by upstream changes

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Oct 2021 01:55:04 +0200

haskell-swish (0.10.0.5-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 10 Aug 2021 19:10:40 +0200

haskell-swish (0.10.0.4-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * simplify source script copyright-check
  * fix update -doc build-dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 19 Jul 2020 22:05:47 +0200

haskell-swish (0.10.0.3-2) unstable; urgency=medium

  * update package relations:
    + add patch 1001 to relax dependency on semigroups;
    + relax GHC upper bounds for semigroups
    + fix really relax GHC upper bounds for polyparse

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 16 Jun 2020 01:39:04 +0200

haskell-swish (0.10.0.3-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update package relations: relax GHC upper bounds for polyparse
  * copyright: extend coverage
  * declare compliance with Debian Policy 4.5.0
  * use debhelper compatibility level 10 (not 9)

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 08 Apr 2020 10:27:37 +0200

haskell-swish (0.10.0.2-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Fix drop GHC relations mtl text (provided by ghc).
    + Fix drop GHC relations network (unneeded since 0.9.2.1).
    + Relax GHC upper bounds for hashable.
    + Fix really drop network-dev upper bound restriction.
  * Update homepage.
  * Update copyright info:
    + Update preferred upstream contact, and alternate source URL.
    + Extend coverage for main upstream author.
    + Extend coverage of packaging.
  * Declare compliance with Debian Policy 4.4.1.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Oct 2019 17:06:52 +0200

haskell-swish (0.10.0.1-2) unstable; urgency=medium

  * Team upload
  * Drop network-dev upper bound restriction on control file

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 28 Aug 2019 13:51:50 +0200

haskell-swish (0.10.0.1-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Bump to support polyparse and its GHC 8.6 compatibility.

  [ Jonas Smedegaard ]
  * Update package relations: Relax GHC upper bounds for polyparse.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 04 Dec 2018 17:07:57 +0100

haskell-swish (0.10.0.0-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Update packages to support building with ghc 8.6
      (increase base and containers limit).

  [ Jonas Smedegaard ]
  * Set Rules-Requires-Root: no.
  * Declare compliance with Debian Policy 4.2.1.
  * Update copyright info: Drop files section for obsolete patches.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Oct 2018 19:05:03 +0200

haskell-swish (0.9.2.1-2) unstable; urgency=medium

  * Simple rebuild.
    Closes: Bug#896974.
  * Update watch file:
    + Fix typo in usage comment.
    + Mention gbp option --upstream-vcs-tag in usage comment.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 May 2018 09:28:49 +0200

haskell-swish (0.9.2.1-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
  0.9.2.0:
    + Initial support for ghc 8.4.
    + Updated to lts 10.8 in stack.yaml so was able to drop extra intern
      dependency version.
    + Now builds with -Wcompat if ghc 8.0 or later is used.
  0.9.2.1:
    + Updated the minimum base package to 4.5.0.0 (GHC 7.4.1), and
      removed some old code for supporting older GHC/package versions.
    + Applied some HLint suggestions.
    + Updated to allow time-1.9.1 (but not time-1.9.0).
    + Updated to lts 11.1 in stack.yaml.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Add usage comment.
    + Bump to version 4.
    + Use substitution variables.
  * Update copyright info:
    + Extend coverage for myself.
    + Rewrap using semantic newlines.
    + Add alternate hg source URL.
    + Extend coverage for main upstream author.
  * Tighten lintian overrides regarding License-Reference.
  * Update Vcs-* fields: Source moved to Salsa.
  * Declare compliance with Debian Policy 4.1.4.
  * Use priority optional (not deprecated extra).
  * Update Maintainer email address.
  * Drop CDBS get-orig-source target: Use "gbp import-orig --uscan"
    instead.
  * Fix add GHC build-dependencies for old-locale.
  * This build rebuilds -doc package.
    Closes: Bug#896974.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 27 Apr 2018 22:35:18 +0200

haskell-swish (0.9.1.10-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    0.9.1.9:
    + Update upper bound on HUnit (only affects tests).
    + Add stack.yaml file.
    0.9.1.10:
    + Update upper bound on time to work with ghc 8.2.1.
    + Updated stack.yaml to use lts 9.1.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Aug 2017 01:51:45 +0200

haskell-swish (0.9.1.8-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Update upper bound on polyparse and directory.
    + Fix build failure of RDFGraphTest with time >= 1.6.
    + Update copyright years in README.md.

  [ Jonas Smedegaard ]
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Stop build-depend on licensecheck.
    + Stop explicitly suppress versioned build-dependency on debhelper.
    + Stop track upstream tarbarll checksum: Use gbp --uscan.
  * Update copyright info:
    + Extend coverage for main upstream author.
    + Extend copyright of packaging to cover current year.
    + Use https protocol in format string.
  * Declare compliance with Debian Policy 4.0.0.
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Update package relations:
    + Drop GHC lower bounds for network-uri: Needed version satisfied
      even in oldsable.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Fix enable use of network-url, as intended since 0.9.1.3-1.
    Drop patch 1001.
  * Drop patches 1002 1003: Fixed upstream.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Consistently use git (not cgit) in paths.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Jun 2017 09:46:37 +0200

haskell-swish (0.9.1.7-4) unstable; urgency=medium

  * Team upload.
  * Bump directory bounds.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 21 Jun 2017 08:16:17 +0200

haskell-swish (0.9.1.7-3) unstable; urgency=medium

  * Modernize CDBS setup: Build-depend on licensecheck (not devscripts).
  * Simply doing this rebuild implies rebuilding against ghc 8.
    Closes: Bug#843404. Thanks to Clint Adams.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 17 Nov 2016 01:18:10 +0100

haskell-swish (0.9.1.7-2) unstable; urgency=medium

  * Unfuzz patch 1001.
  * Add patch to relax dependency on polyparse.
    Relax build-dependency accordingly.
    Thanks to Sean Whitton.
  * Declare compliance with Debian Policy 3.9.8.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 29 May 2016 17:35:09 +0200

haskell-swish (0.9.1.7-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.7.
  * Update copyright info:
    + Extend copyright for main upstream author to cover current year.
    + Extend copyright of packaging to cover current year.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 05 Feb 2016 13:24:56 +0100

haskell-swish (0.9.1.5-2) unstable; urgency=medium

  * Fix (re-)drop build-dependency (accidentally reverted in 0.9.1.5-1).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 26 Dec 2015 19:51:40 +0530

haskell-swish (0.9.1.5-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Fix source URL.
    + Extend coverage for main upstream author.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Relax inclusion of haskell-devscript snippet: Unneeded for clean
    target.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Add lintian override regarding debhelper 9.
  * Update package relations:
    + Relax GHC (build-)dependencies on semigroups.
    + Temporarily avoid build-depending on libghc-nats-doc
      (see bug#809032).
  * Strip hardcoded rpath of swish binary.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 26 Dec 2015 19:16:01 +0530

haskell-swish (0.9.1.3-2) unstable; urgency=medium

  * Drop build-dependency on libghc-transformers-doc, unnecessary with GHC
    7.8.

 -- Colin Watson <cjwatson@debian.org>  Tue, 12 May 2015 01:25:47 +0100

haskell-swish (0.9.1.3-1) unstable; urgency=medium

  * Move packaging to pkg-haskell Alioth group.
  * Update copyright info: Extend coverage for myself.
  * Bump debhelper compatibility level to 9.
  * Enable use of network-uri.
    Update package relations:
    + Tighten GHC (build-)dependencies on network.
    + Add GHC (build-)dependencies on network-uri.
    + Relax GHC (build-)dependencies on polyparse and semigroups.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 29 Apr 2015 12:49:13 +0200

haskell-swish (0.9.1.0-1) experimental; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Relax GHC (build-)dependencies on text.
    + Drop lower bounds where no older version exists in Debian.
  * Update copyright info:
    + Extend coverage for main upstream author.
  * Declare compliance with Debian Policy 3.9.6.
  * Update Vcs-Browser URL to use cgit web frontend.
  * Fix use canonical Vcs-Git URL.
  * Set flag to avoid newer network-uri than available in Debian.
  * Add patch 1001 to avoid bogus dependency on old network-uri.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 21 Dec 2014 14:37:21 +0100

haskell-swish (0.9.0.15-1) unstable; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Tue, 16 Sep 2014 15:21:51 -0400

haskell-swish (0.9.0.14-1) unstable; urgency=medium

  [ Jonas Smedegaard ]
  * Fix update watch file to use Hackage distro-monitor.

  [ Clint Adams ]
  * New upstream version.

 -- Clint Adams <clint@debian.org>  Tue, 29 Apr 2014 22:47:01 -0400

haskell-swish (0.9.0.12-1) unstable; urgency=medium

  * New upstream version.
  * Bump to Standards-Version 3.9.5.

 -- Clint Adams <clint@debian.org>  Sun, 20 Apr 2014 10:15:26 -0400

haskell-swish (0.9.0.7-1) unstable; urgency=low

  [ upstream ]
  * New release.
    + Turtle parser: updated to the Candidate Recommendation (19
      February 2013) specification; added minor improvements to error
      messages when given invalid syntax. As part of the upgrade, there
      is no longer a default namespace set up for the empty prefix and
      numeric literals are no-longer converted into a canonical form.
    + Turtle/N3 output: improved string formatting (better handling of
      string literals with three or more consecutive @\"@ characters);
      blank node handling has been improved but the output may not be as
      elegant.
    + NTriples parser: now accepts upper-case language tags such as
      @en-UK@ (case is preserved).
    + @Swish.QName.LName@ names can now contain @#@, @:@ and @/@
      characters.
    + Added tests for the Turtle parser and formatter.
    + The new @w3ctests@ flag will build the @runw3ctests@ executable,
      which will run the W3C Turtle tests (if available locally).
    + Minor fixes and additions to the documentation.

  [ Jonas Smedegaard ]
  * Extend copyright coverage for main upstream author.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 15 Sep 2013 20:17:25 +0200

haskell-swish (0.9.0.3-1) unstable; urgency=low

  * Initial packaging.
    Closes: bug#718018.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 27 Jul 2013 22:59:10 +0200
